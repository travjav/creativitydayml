import pandas as pd
import matplotlib as plt
import keras
from keras.callbacks  import ModelCheckpoint
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation

df = pd.read_csv('./TRAINING_DATA/r1.csv')

x = df[['c1','c2','c3','c4','c5','c6','c7']]
y = df['value'] = pd.to_numeric(df['value'], errors='coerce')

y = keras.utils.to_categorical(y, 2)

model = Sequential()
model.add(Dense(252, input_shape=(7,), activation='tanh'))
model.add(Dropout(0.7))

model.add(Dense(252, activation='tanh'))
model.add(Dropout(0.7))

model.add(Dense(252, activation='tanh'))
model.add(Dropout(0.7))

model.add(Dense(252, activation='tanh'))
model.add(Dropout(0.7))

model.add(Dense(252, activation='tanh'))
model.add(Dropout(0.7))

model.add(Dense(2, activation='softmax'))

model.compile(loss='binary_crossentropy',
             optimizer='adam',
             metrics=['accuracy'])

filepath = 'model.h5'
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]
X_train, X_test, Y_train, Y_test = train_test_split(x, y, test_size=0.20, random_state=42, shuffle=True)
# Fit the model
hist = model.fit(X_train, Y_train, validation_split=0.20, epochs=25, batch_size=32, callbacks=callbacks_list, verbose=1)
# Evaluate the model
loss, accuracy = model.evaluate(x, y, verbose=0)
print('Accuracy: %f' % (accuracy * 100))
print('***** TOTAL TRAINING SET AMOUNT', X_train.shape, Y_train.shape)
print('***** TEST', X_test.shape, Y_test.shape)
print(model.summary())




# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")

# Show chart(s)
loss = hist.history['loss']
val_loss = hist.history['val_loss']
epochs = range(1, len(loss) + 1)
plt.plot(epochs, loss, 'b', label='Training loss', linestyle='solid')
plt.plot(epochs, val_loss, 'r', label='Validation loss', linestyle='solid')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.grid(True)
plt.legend()
plt.style.use(['classic'])
plt.show()

train_acc = hist.history['acc']
val_acc = hist.history['val_acc']
epochs = range(1, len(loss) + 1)
plt.plot(epochs, loss, 'b', label='Training loss', linestyle='solid')
plt.plot(epochs, train_acc, 'g', label='Training Accuracy', linestyle='solid')
plt.plot(epochs, val_acc, 'r', label='Validation Accuracy', linestyle='solid')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.grid(True)
plt.legend()
plt.style.use(['classic'])
plt.show()

