import pandas as pd
import matplotlib as plt
import keras
from keras.callbacks  import ModelCheckpoint
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.models import model_from_json


